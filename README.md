# OpenQVision
OpenQVision is an open source object detection software based on PyQt5 and OpenCV.
![alt text](https://github.com/martuscellifaria/OpenQVision/blob/main/OpenQVision-Thumbnail.png)

The output on Telegram, on the other hand, will be something like this:
![alt text](https://github.com/martuscellifaria/ObjectDetection/blob/master/output_on_telegram.jpg)

### Installation
##### Prerequisites
- [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Python Libraries] QDarkStyle==3.0.2 PySide2==5.15.2 python-dotenv==0.15.0 opencv-python-headless==4.5.2.52 requests==2.24.0 PyQt5==5.15.4
- Developed and tested in Ubuntu 20.04

##### Clone the project

```sh
$ git clone https://github.com/martuscellifaria/OpenQVision.git
```
#####How it works:
You can directly start your camera, or you should browse neural network weights and config files to OpenQVision before starting the camera.

For more instructions, see the video on YouTube in the link down below.
https://www.youtube.com/watch?v=mJ0oHXq7gFY
