from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication
import sys
import OpenQVision
import qdarkstyle

class ExampleApp(QtWidgets.QMainWindow, OpenQVision.Ui_OpenQVision):
    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())

def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
