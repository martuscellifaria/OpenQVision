import cv2 as cv
import sys, os
from starman_jr import send_photo
from dotenv import load_dotenv
import time
import numpy as np

def process_and_find(img, net, classNames, targetClassName):
    blob = cv.dnn.blobFromImage(img, 1/255, (416,416), (0,0,0), True, crop=False)
    net.setInput(blob)

    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    layerOutputs = net.forward(ln)
    boxes = []
    confidences = []
    classIDs = []
    confidence_threshold=0.75
    overlapping_threshold=0.3
    W = None
    H = None
    if W is None or H is None:
        (H, W) = img.shape[:2]
    for output in layerOutputs:
        for detection in output:
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]
            if confidence > confidence_threshold:
                # Scale the bboxes back to the original image size
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                classIDs.append(classID)
    done = 'no'
    # Remove overlapping bounding boxes and boundig boxes
    bboxes = cv.dnn.NMSBoxes(
        boxes, confidences, confidence_threshold, overlapping_threshold)
    if len(bboxes) > 0:
        for i in bboxes.flatten():
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])
            cv.rectangle(img, (x, y), (x + w, y + h), color=(0,0,0), thickness=1)
            text = "{}: {:.4f}".format(classNames[classIDs[i]], confidences[i])
            cv.putText(img, text, (x, y - 5),
                        cv.FONT_HERSHEY_SIMPLEX, 0.5, color=(0,0,0), thickness=1)
            if (classNames[classID-1] == targetClassName):
                print(f'Found the {targetClassName.capitalize()}')
                cv.imwrite(f'temp/{targetClassName}.png', cv.cvtColor(img, cv.COLOR_BGR2RGB))
                load_dotenv()
                telegram = os.getenv('telegram')
                send_photo(f'temp/{targetClassName}.png', f'Found the {targetClassName.capitalize()}!', telegram)
                os.remove(f'temp/{targetClassName}.png')
                done = 'yes'
    return img, done